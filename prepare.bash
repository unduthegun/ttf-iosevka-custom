#!/usr/bin/env bash
# usage: prepare.bash package_version

# delete current instructions in toml
sed -i -e '/# ### Configuring Custom Build/q' private-build-plans.toml

wget -qO- "https://raw.githubusercontent.com/be5invis/Iosevka/$1/README.md" | sed '0,/### Configuring Custom Build/d' | sed '/#### Metric Override/{Q}' | sed '/^<!--/d' | sed '$!N; /^\(.*\)\n\1$/!P; D' | sed 's/^/# /' >> private-build-plans.toml
